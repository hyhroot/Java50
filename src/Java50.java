
/**
 * 目录：
 * 01：判断101-200之间有多少个素数，并输出所有素数
 * 02、打印出所有的"水仙花数"，
 * 03、古典问题：有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少？
 * 04、
 * 05、
 * 06、
 * 07、
 * 08、
 * 09、
 * 10、
 * 11、
 * 12、
 * <p>
 * 题目：判断101-200之间有多少个素数，并输出所有素数
 *
 * @author HYH
 * 素数的定义：质数又称素数。一个大于1的自然数，除了1和它自
 * 身外，不能整除其他自然数的数叫做质数；
 * 分析：首先排除偶数（101-200之间），然后除以从3到自己本身的奇数（求余），
 * 如果全都不能被整除，则输出该数（即为素数）
 */
public class Java50 {
    static void count(int i) {
        System.out.println("\n---------第" + i + "题-----------:\n");
    }

    /*第一题：
     *题目：判断101-200之间有多少个素数，并输出所有素数
     * @author HYH
     * 素数的定义：质数又称素数。一个大于1的自然数，除了1和它自
     * 身外，不能整除其他自然数的数叫做质数；
     * 分析：首先排除偶数（101-200之间），然后除以从3到自己本身的奇数（求余），
     * 如果全都不能被整除，则输出该数（即为素数）
     */
    static void java01() {
        count(1);
        for1:
        for (int i = 101; i <= 200; ) {    //for1：循环标签
            for2:
            for (int j = 3; j <= (i / 2 + 1); j++) {
                if (i % j == 0) {
                    break for2;    //跳出for2循环
                }
                if (j == i / 2 + 1)
                    System.out.println(i);
            }
            i += 2;//直接排除偶数；
        }
    }

    /**
     * 第二题：
     * 题目：打印出所有的"水仙花数"，
     * 所谓"水仙花数"是指一个三位数，其各位数字立方和等于该数本身。例如：
     * 153是一个"水仙花数"，因为153=1的三次方＋5的三次方＋3的三次方。
     *
     * @author HYH
     * 分析：水仙花数为三位数，既100-999；
     * 利用for循环挨个计算，先分别求出每个三位数的各位、十位、百位
     * 然后分别3次方后相加，再与原数字比较是否相等。
     */
    static void java02() {
        count(2);
        for (int i = 100; i <= 999; i++) {
            int t[] = disassemble(i, true);
            if (i == (t[0] + t[1] + t[2])) {
                System.out.println(i + "是水仙花数");
            }
        }
    }

    //分别获取每个三位数的各位、十位、百位
    //boo参数为true则返回各位数的三次幂、十位的三次幂、百位的三次幂
    //boo参数为false则返回个位数、十位数、百位数
    static int[] disassemble(int i, boolean boo) {
        int a[] = new int[3];
        int unitsDigit = 0;//个位数
        int tensDigit = 0;//十位数
        int hundredsDigit = 0;//百位数

        if (!boo) {
            a[0] = unitsDigit = i / 100;
            a[1] = tensDigit = (i / 10) % 10;
            a[2] = hundredsDigit = i % 10;

            return a;
        } else {
            a[0] = unitsDigit = i / 100;
            a[1] = tensDigit = (i / 10) % 10;
            a[2] = hundredsDigit = i % 10;

            for (int j = 0; j < 3; j++) {
                a[j] = (int) Math.pow(a[j], 3);
            }
            return a;
        }
    }

    /**
     * 第三题
     * 题目：<tt>古典问题：</t>有一对兔子，从出生后第3个月起每个月
     * 都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，
     * 假如兔子都不死，问每个月的兔子总数为多少？
     *
     * @param m 月份
     * @author HYH
     * 程序分析：
     * 1  2  3  4  5  6   7
     * 1  1  2  5  8  13  21
     */

    static void java03(int m) {
        count(3);
        int rabb[] = new int[m];
        int sum = 1;
        rabb[0] = 1;
        rabb[1] = 1;
        if (m == 1 || m == 2) {
            System.out.println("第" + m + "月兔子个数为1");
        } else {
            for (int i = 2; i < m; i++) {
                rabb[i] = rabb[i - 1] + rabb[i - 2];
            }
        }
        int temp = 1;
        for (int i : rabb) {
            System.out.println("第" + temp + "个月兔子个数：" + i);
            temp++;
        }
    }

    /**
     * 第四题：
     * 将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。
     * <p>
     * 9 = 1*3*3
     *
     * @author HYH
     */
    static void java04(int a) {
        if (a >= 4) {
            for (int i = 2; i <= (a / 2) + 1; i++) {
                int c = 0;
                if (a % i == 0) {
                    c = a / i;
                    System.out.print(i + "*");
                    java04(c);
                    break;
                }
                if (i == a / 2) {
                    System.out.println(a);
                }
            }
        } else {
            System.out.println(a);
        }
    }

    /**
     * 第五题：
     * 利用条件运算符的嵌套来完成此题：学习成绩>=90分的同学用A表示，60-89分之间的用B表示，
     * 60分以下的用C表示。
     *
     * @author HYH
     */
    static void java05(int a) {
        char c = (a >= 90) ? 'A' : ((a >= 60) ? 'B' : 'C');
        System.out.println(c);
    }

    /**
     * 第六题：题目：输入两个正整数m和n，求其最大公约数和最小公倍数。
     * 1.程序分析：利用辗除法。
     * @param m
     * @param n
     */

    static void java06(int m, int n) {
        int max = Math.max(m, n);
        int min = Math.min(m, n);

        while (true) {
            if (max % min == 0) {
                System.out.println("最大公约数为：" + min);
                if(min==n){
                System.out.println("最小公倍数为m："+m);
                }else {
                    if (min==1){
                    System.out.println("最小公倍数为n*m："+m*n);
                    }else {
                        System.out.println("最小公倍数为min*m："+m*min);
                    }
                }
                break;
            } else {
                int temp = min;
                min = max % temp;
                max = temp;
                if (min == 0) {
                    System.out.println("最大公约数为11：1");
                    System.out.println("最小公倍数为："+(m*n));
                    break;
                }
            }
        }
    }


    /**
     * main 方法
     * @param args
     */
    public static void main(String[] args) {
//        java01();
//        java02();
//        java03(5);
//        java04(5);
//        java05(50);
        java06(124, 99);
    }

}

